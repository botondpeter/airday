package com.hp.tools.judge.utils;

/**
 * @author Octavian
 * @since 17.03.2015
 */
public class ConnectionDetails {
    private String protocol;
    private String host;
    private int port;
    private String path;
    private long connectTimeout; //milliseconds
    private long readTimeout; //milliseconds

    public ConnectionDetails(String protocol, String host, int port, String path, long connectTimeout, long readTimeout) {
        this.protocol = protocol;
        this.host = host;
        this.port = port;
        this.path = path;
        this.connectTimeout = connectTimeout;
        this.readTimeout = readTimeout;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(long connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public long getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(long readTimeout) {
        this.readTimeout = readTimeout;
    }

    public String getUrl() {
        return protocol + "://" + host + ":" + port + "/" + path;
    }
}
