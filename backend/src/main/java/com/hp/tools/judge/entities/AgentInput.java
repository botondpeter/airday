package com.hp.tools.judge.entities;

/**
 * @author Octavian
 * @since 20.02.2015
 */
public class AgentInput {
    private String language;
    private String code;
    private String testCode;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTestCode() {
        return testCode;
    }

    public void setTestCode(String testCode) {
        this.testCode = testCode;
    }
}
