package com.hp.tools.judge.web;

import com.hp.tools.judge.entities.ProgrammingProblemDetails;
import com.hp.tools.judge.exceptions.OnlineJudgeException;
import com.hp.tools.judge.services.JudgeService;
import com.hp.tools.judge.utils.Constants;
import com.wordnik.swagger.annotations.*;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Octavian
 * @since 01.02.2015
 */
@Singleton
@Path("/problems")
@Api(value = "Problems", position = 1)
public class ProblemResource {
    private JudgeService service = Constants.JUDGE_SERVICE;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Get problems details", notes = "Return all the problems with the corresponding score for a given user", response = ProgrammingProblemDetails.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "The user ID is invalid")
    })
    public Response getProblems(
            @ApiParam(value = "The user ID")
            @QueryParam("userId")
            int userId) {
        try {
            return Response.ok(service.getAllProgrammingProblemDetails(userId)).build();
        } catch (OnlineJudgeException e) {
            return Response.status(400).entity(e.getMessage()).build();
        }
    }
}
