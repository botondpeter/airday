import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class Problem1UnitTest {

    private static ProblemSolution ps;
    @BeforeClass
    public static void setup(){
        ps=new ProblemSolution();
    }

    @Test
    public void testReverse1(){
        assertEquals("cba", ps.reverse("abc"));
    }

    @Test
    public void testReverse2(){
        assertEquals("aba", ps.reverse("aba"));
    }

    @Test
    public void testReverse3(){
        assertEquals("aaa", ps.reverse("aaa"));
    }

    @Test
    public void testReverse4(){
        assertNotEquals("abcd", ps.reverse("abcd"));
    }

    @Test
    public void testReverse5(){
        assertEquals("aaaaaaiiiiiaaaaisssodff", ps.reverse("ffdosssiaaaaiiiiiaaaaaa"));
    }
}
