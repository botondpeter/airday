from UUTModule import *
import unittest
class StringReverseTests(unittest.TestCase):
    def testReverse1(self):
        self.assertEquals("cba", reverse("abc"))


    def testReverse2(self):

        self.assertEquals("aba", reverse("aba"))


    def testReverse3(self):

        self.assertEquals("aaa", reverse("aaa"))


    def testReverse4(self):

        self.assertNotEquals("abcd", reverse("abcd"))


    def testReverse5(self):

        self.assertEquals("aaaaaaiiiiiaaaaisssodff", reverse("ffdosssiaaaaiiiiiaaaaaa"))



def main():
    unittest.main()

if __name__ == '__main__':
    main()