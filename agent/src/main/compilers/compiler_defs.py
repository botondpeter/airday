import os
import py_compile
import re
import subprocess
import tempfile
import threading

from main.exception.exception_defs import CompilationError, TimeoutError


TIMEOUT = 120


def run_popen_with_timeout(command_string, timeout, env):
    kill_check = threading.Event()

    def _kill_process_after_a_timeout(p):
        p.terminate()
        kill_check.set()  # tell the main routine that we had to kill
        # use SIGKILL if hard to kill...
        return

    p = subprocess.Popen(command_string, env=env, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    watchdog = threading.Timer(timeout, _kill_process_after_a_timeout, args=(p, ))
    watchdog.start()
    (stdout, stderr) = p.communicate()
    watchdog.cancel()  # if it's still waiting to run
    success = not kill_check.is_set()
    kill_check.clear()
    if success:
        return (p.returncode, stdout, stderr)
    else:
        raise TimeoutError("Timeout Error")


class DefaultCompiler(object):
    def __init__(self, code=None, unit_test=None):
        self.code = code
        self.unit_test = unit_test

    def compile_code(self):
        pass

    def run_test(self):
        pass


class JavaCompiler(DefaultCompiler):
    def __init__(self, code=None, unit_test=None):
        super(JavaCompiler, self).__init__(code, unit_test)
        self.temp_directory = tempfile.mkdtemp(prefix="tmpCompile")

        self.class_name = self.detect_class_name(self.code)
        if not self.class_name:
            raise CompilationError("No class defined in source code file")
        self.code_file = self.temp_directory + "\\" + self.class_name + ".java"
        with open(self.code_file, 'w') as f:
            f.write(self.code)

        self.unit_class_name = self.detect_class_name(self.unit_test)
        if not self.unit_class_name:
            raise CompilationError("No unit test class defined in source code file")
        self.unit_test_file = self.temp_directory + "\\" + self.unit_class_name + ".java"
        with open(self.unit_test_file, 'w') as f:
            f.write(self.unit_test)

    def compile_code(self):
        java_home = os.getenv("JAVA_HOME")
        classpath = "../../../libs/javalib/*;" + self.temp_directory + "/."
        cmd = "javac -cp " + classpath + " " + self.temp_directory + "/*.java"
        proc = subprocess.Popen(cmd, env={'PATH': java_home + "\\bin"}, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (stdout, stderr) = proc.communicate()
        return (proc.returncode, stdout, stderr)

    def run_test(self):
        java_home = os.getenv("JAVA_HOME")
        classpath = "../../../libs/javalib/*;" + self.temp_directory + "/."
        cmd = "java -cp " + classpath + " " + "org.junit.runner.JUnitCore " + self.unit_class_name
        environment = {'PATH': java_home + "\\bin"}
        (returncode, stdout, stderr) = run_popen_with_timeout(cmd, TIMEOUT, environment)
        return (returncode, stdout, stderr)

    def detect_class_name(self, class_definition):
        regex = re.compile("public\\s(?:class|interface)\\s(.*?)\\s(?:.*){")
        found = regex.search(class_definition)
        return found.group(1)


class PythonCompiler(DefaultCompiler):
    def __init__(self, code=None, unit_test=None):
        super(PythonCompiler, self).__init__(code, unit_test)
        self.temp_directory = tempfile.mkdtemp(prefix="tmpCompile")

        self.code_file = self.temp_directory + "\\" + "UUTModule.py"
        with open(self.code_file, 'w') as f:
            f.write(self.code)

        self.unit_test_file = self.temp_directory + "\\" + "UnitTest.py"
        with open(self.unit_test_file, 'w') as f:
            f.write("from UUTModule import *\n" + self.unit_test)

    def compile_code(self):
        try:
            py_compile.compile(self.code_file, doraise=True)
        except py_compile.PyCompileError as ex:
            return (-1, "", ex.message)
        return (0, "", "")


    def run_test(self):
        cmd = "python " + self.unit_test_file
        (returncode, stdout, stderr) = run_popen_with_timeout(cmd, TIMEOUT, None)
        return (returncode, stdout, stderr)
